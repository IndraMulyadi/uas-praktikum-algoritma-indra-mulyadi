#include <stdio.h>

double hitung_nilai(double absen, double tugas, double quiz, double uts, double uas) {
    return ((0.1 * absen) + (0.2 * tugas) + (0.3 * quiz) + (0.4 * uts) + (0.5 * uas)) / 2;
}

char hitung_huruf_mutu(double nilai) {
    char Huruf_Mutu;

    if (nilai > 85 && nilai <= 100)
        Huruf_Mutu = 'A';
    else if (nilai > 70 && nilai <= 85)
        Huruf_Mutu = 'B';
    else if (nilai > 55 && nilai <= 70)
        Huruf_Mutu = 'C';
    else if (nilai > 40 && nilai <= 55)
        Huruf_Mutu = 'D';
    else if (nilai >= 0 && nilai <= 40)
        Huruf_Mutu = 'E';

    return Huruf_Mutu;
}

int main() {
    double nilai, quiz, absen, uts, uas, tugas;
    char Huruf_Mutu;

    quiz = 40; absen = 100; uts = 60; uas = 50; tugas = 80;

    printf("Absen = %.2f UTS = %.2f\n", absen, uts);
    printf("Tugas = %.2f UAS = %.2f\n", tugas, uas);
    printf("Quiz = %.2f\n", quiz);

    nilai = hitung_nilai(absen, tugas, quiz, uts, uas);
    Huruf_Mutu = hitung_huruf_mutu(nilai);

    printf("Huruf Mutu : %c\n", Huruf_Mutu);

    return 0;
}
